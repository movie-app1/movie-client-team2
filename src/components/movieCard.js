import React from "react";
import { Link } from "react-router-dom";
function movieCard(props) {
    const { movie } = props;
  return (
    <div className="cardss">
      <div className="cards-wrapper">
        <div className="carddd d-none d-md-block">
        <img
            src={movie.image}
            className="card-img-top"
            alt="..."
            />
        <div className="card-body bg-dark text-light text-light">
            <h5 className="card-title p-3">
             <b>{movie.title}</b>
            </h5>

            <Link to="" className="btn btn-primary mt-2 mb-4">
              Play
            </Link>
            <Link
              to="/detail"
              className="btn btn-primary mx-2 mt-2 mb-4"
            >
              Detail
            </Link>
        </div>
        </div>
      </div>
    </div>
  );
}

export default movieCard;
