import React, { useEffect, useState } from "react";
import movieCard from "../components/movieCard";
import axios from "axios";
import "../Styles/home.css";
import { Link } from "react-router-dom";

function Home() {
  // const [movies, setMovie] = useState([]);

  // const fetchMovies = async () => {
  //   try {
  //     const { data, status } = await axios.get(
  //       "http://localhost:8080/api/movie",
  //       {
  //         headers: {
  //           Authorization: `Bearer ${localStorage.getItem("token")}`,
  //         },
  //       }
  //     );
  //     if (status === 200) {
  //       setMovie(data);
  //       // console.log(data);
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  // useEffect(() => {
  //   fetchMovies();
  // }, []);

  return (
    <div className="home">
      <div className="carousel">
        <ul className="slides ">
          <input type="radio" name="radio-buttons" id="img-1" checked />
          <li className="slide-container">
            <div className="slide-image">
              <img src="https://www.wallpaperup.com/uploads/wallpapers/2013/11/21/177868/8668fc005a55b7278d91df69efbe6fad.jpg" />
            </div>
            <div className="carousel-controls">
              <label htmlFor="img-3" className="prev-slide">
                <span>&lsaquo;</span>
              </label>
              <label htmlFor="img-2" className="next-slide">
                <span>&rsaquo;</span>
              </label>
            </div>
          </li>
          <input type="radio" name="radio-buttons" id="img-2" checked />
          <li className="slide-container">
            <div className="slide-image">
              <img src="https://a-static.besthdwallpaper.com/tomorrowland-the-modern-world-wallpaper-3554x1999-11826_53.jpg" />
            </div>
            <div className="carousel-controls">
              <label htmlFor="img-1" className="prev-slide">
                <span>&lsaquo;</span>
              </label>
              <label htmlFor="img-3" className="next-slide">
                <span>&rsaquo;</span>
              </label>
            </div>
          </li>
          <input type="radio" name="radio-buttons" id="img-3" checked />
          <li className="slide-container">
            <div className="slide-image">
              <img src="https://a-static.besthdwallpaper.com/alone-in-city-wallpaper-3554x1999-81342_53.jpg" />
            </div>
            <div className="carousel-controls">
              <label htmlFor="img-2" className="prev-slide">
                <span>&lsaquo;</span>
              </label>
              <label htmlFor="img-1" className="next-slide">
                <span>&rsaquo;</span>
              </label>
            </div>
          </li>
          <div className="carousel-dots">
            <label
              htmlFor="img-1"
              className="carousel-dot"
              id="img-dot-1"
            ></label>
            <label
              htmlFor="img-2"
              className="carousel-dot"
              id="img-dot-2"
            ></label>
            <label
              htmlFor="img-3"
              className="carousel-dot"
              id="img-dot-3"
            ></label>
          </div>
        </ul>
      </div>
      <h3 className="tv text-white text-start">Vision+ Originals</h3>
      <div className="container">
        <div className="row">
          <div className="col-lg-3">
            <div class="card">
              <img
                class="card-img-top"
                src="https://static.mncnow.id/images/series/ba336dbd/1cac_r32.jpg"
                alt="Card image cap"
              />
              <div class="card-body bg-dark">
                <h5 class="card-title text-light">Cidro Asmara</h5>

                <Link to="#" className="btn btn-primary mt-2 mb-4">
                  Play
                </Link>

                <Link to="#" className="btn btn-primary mx-2 mt-2 mb-4">
                  Detail
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div class="card">
              <img
                class="card-img-top"
                src="https://static.mncnow.id/images/series/ba336dbd/1cac_r32.jpg"
                alt="Card image cap"
              />
              <div class="card-body bg-dark">
                <h5 class="card-title text-light">Cidro Asmara</h5>

                <Link to="#" className="btn btn-primary mt-2 mb-4">
                  Play
                </Link>

                <Link to="#" className="btn btn-primary mx-2 mt-2 mb-4">
                  Detail
                </Link>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div class="card">
              <img
                class="card-img-top"
                src="https://static.mncnow.id/images/series/ba336dbd/1cac_r32.jpg"
                alt="Card image cap"
              />
              <div class="card-body bg-dark">
                <h5 class="card-title text-light">Cidro Asmara</h5>

                <Link to="#" className="btn btn-primary mt-2 mb-4">
                  Play
                </Link>

                <Link to="#" className="btn btn-primary mx-2 mt-2 mb-4">
                  Detail
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div class="card text-light bg-dark">
        <img
          src="https://static.mncnow.id/images/series/ba336dbd/1cac_r32.jpg"
          class="card-img-top"
          alt="..."
          style={{ height: "200px", width: "350px" }}
        />
        <div class="card-body">
          <h5 class="card-title">Cidro Asmara</h5>

          <Link to="#" className="btn btn-primary mt-2 mb-4">
            Play
          </Link>

          <Link to="#" className="btn btn-primary mx-2 mt-2 mb-4">
            Detail
          </Link>
        </div>
      </div> */}
      {/* <div className="cardss">
        <div
          id="carouselExampleControls"
          className="carousel slide"
          data-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="cards-wrapper">
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/ba336dbd/1cac_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light text-light">
                    <h5 className="card-title p-3">
                      <b>Cidro Asmoro</b>
                    </h5>

                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/vod/679f27a2/5bfc_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Jujur</b>
                    </h5>

                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/55a0ce2c/00ba_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Kejarlah Daku</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/017e820e/efe0_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Catatan Sekolah</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/vod/e863580c/39a9_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Sebelum Dunia</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>

                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/vod/58b365b0/ed5f_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Ada Dewa</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="carousel-item">
              <div className="cards-wrapper">
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/vod/ce618086/34bc_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>4 Sekawan</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/ec021e73/eefb_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Piknik Pesona</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/33d5fca7/1bcf_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>My Comic</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/vod/0b7c6d4a/683c_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Melody of Love</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/32421dd8/adc2_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>12 Hari</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/2e8791d4/ce5c_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Menggapai Cinta</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="carousel-item">
              <div className="cards-wrapper">
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/9db053ea/455f_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Royal Blood</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/95d66b80/732c_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Bad Parenting</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/6f892718/fa1e_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Cinta Balik Awan</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/a5586152/614c_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Daur Hidup</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/vod/1dc4dac0/f0fb_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Orkes Semesta</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
                <div className="carddd d-none d-md-block">
                  <img
                    src="https://static.mncnow.id/images/series/27ae709a/6f81_r32.jpg"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body bg-dark text-light">
                    <h5 className="card-title p-3">
                      <b>Menemukan-Mu</b>
                    </h5>
                    <a to="#" className="btn btn-primary mt-2 mb-4">
                      Play
                    </a>
                    <Link
                      to="/detail"
                      className="btn btn-primary mx-2 mt-2 mb-4"
                    >
                      Detail
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <a
            className="carousel-control-prev"
            to="#carouselExampleControls"
            role="button"
            data-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next"
            to="#carouselExampleControls"
            role="button"
            data-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div> */}

      <h3 className="fav text-light text-start mt-4">Your favorite Live TV</h3>
      <div className="container">
        <div className="row cenel mt-4">
          <div className="col-2">
            <img
              className="bg-light w-75 rounded-circle mx-2"
              src="https://static.mncnow.id/images//channel/928ba7e3/38c2.png"
              alt="Avatar"
            />
          </div>
          <div className="col-2">
            <img
              className="bg-light w-75 rounded-circle mx-2"
              src="https://static.mncnow.id/images//channel/ba065727/a4fd.png"
              alt="Avatar"
            />
          </div>
          <div className="col-2">
            <img
              className="bg-light w-75 rounded-circle mx-2"
              src="https://static.mncnow.id/images//channel/ef3eed68/040d.png"
              alt="Avatar"
            />
          </div>
          <div className="col-2">
            <img
              className="bg-light w-75 rounded-circle mx-2"
              src="https://static.mncnow.id/images//channel/8f231a4a-387.png"
              alt="Avatar"
            />
          </div>
          <div className="col-2">
            <img
              className="bg-light w-75 rounded-circle mx-2"
              src="https://static.mncnow.id/images//channel/ab4eed7b/ddc6.png"
              alt="Avatar"
            />
          </div>
          <div className="col-2">
            <img
              className="bg-light w-75 rounded-circle mx-2"
              src="https://static.mncnow.id/images//channel/66544aea-676.png"
              alt="Avatar"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
