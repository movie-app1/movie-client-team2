import { instance as axios } from "../utils/Api";
import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

function Game({ games }) {
  const fetchMovies = async () => {
    try {
      const { data, status } = await axios.get(`menus/id/${games.id}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      if (status === 200) {
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchMovies();
  }, []);

  const removePost = async () => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`game/delete/${games.id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  return (
    <div>
      <div className="container mt-5 mb-5 games">
        <div
          className="d-flex justify-content-center row"
          style={{ marginTop: "100px" }}
        >
          {/* <div className="col-md-10"> */}
          <div className="row p-2 bg-white border rounded">
            <div className="col-md-3 mt-1">
              <img
                className="img-fluid img-responsive rounded product-image"
                src={games.image}
              />
            </div>
            <div className="col-md-6 mt-1 align-self-start">
              <div className="text-start mx-auto text-dark">
                <p className="fs-5 ">{games.title}</p>
                <p> {games.description}</p>
              </div>
              <br />
              <br />
            </div>
            <div className="col-md-3 mt-1 align-self-center d-flex justify-content-center">
              <button className="justify-content-center mx-auto btn btn-warning ">
                Play Now
              </button>
              {localStorage.getItem("role") === "admin" ? (
                <Link to={`/editGame/${games.id}`}>
                  <button className="justify-content-center mx-auto btn btn-success ">
                    Edit
                  </button>
                </Link>
              ) : (
                <></>
              )}
              {localStorage.getItem("role") === "admin" ? (
                <button className="justify-content-center mx-auto btn btn-danger ">
                  <div onClick={removePost}>
                    <i class="fa-solid fa-trash"></i>
                  </div>
                </button>
              ) : (
                <></>
              )}
            </div>
          </div>
          {/* </div> */}
        </div>
      </div>
    </div>
  );
}

export default Game;
