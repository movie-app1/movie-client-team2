import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { instance as axios } from "../utils/Api";
import Swal from "sweetalert2";

function TvShow({ channel }) {
  const navigate = useNavigate();

  const fetchMovies = async () => {
    try {
      const { data, status } = await axios.get(`menus/id/${channel.id}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      if (status === 200) {
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchMovies();
  }, []);

  const removePost = async () => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: "Cancel",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`channel/delete/${channel.id}`, {});
        navigate("/tvShow");
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div>
      {" "}
      <div className="card text-light bg-dark" style={{ width: "250px" }}>
        <img
          src={channel.image}
          class="card-img-top"
          alt="..."
          style={{ width: "250px" }}
        />
        <div className="card-body ">
          <h5 className="card-title">{channel.title}</h5>
          <div className="row">
            {localStorage.getItem("role") === "admin" ? (
              <div className="btn btn-warning mt-2 mb-4 col-lg-3">
                <Link to={`/editChannel/${channel.id}`}>
                  <i className="fa-solid fa-pen-to-square"></i>
                </Link>
              </div>
            ) : (
              <></>
            )}
            <div className="btn btn-primary mt-2 mb-4 col-lg-3 ">
              <i className="fa-solid fa-play"></i>
            </div>
            {localStorage.getItem("role") === "admin" ? (
              <div
                onClick={removePost}
                className="btn btn-danger mt-2 mb-4 col-lg-3"
              >
                <i className="fa-solid fa-trash"></i>
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default TvShow;
