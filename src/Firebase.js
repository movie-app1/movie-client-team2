// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB01KXE2kp4wWvPvpsYb8Zkk-LkZC4nyo4",
  authDomain: "app-movie-e880a.firebaseapp.com",
  projectId: "app-movie-e880a",
  storageBucket: "app-movie-e880a.appspot.com",
  messagingSenderId: "139990334961",
  appId: "1:139990334961:web:9c098da17966df9fe85850",
  measurementId: "G-B270SQW1BZ",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Firebase
export const storage = getStorage(app);
// const analytics = getAnalytics(app);
