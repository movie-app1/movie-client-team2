import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import TvShow from "../components/TvShow";
import { instance as axios } from "../utils/Api";
import "../Styles/TvShow.css";

function TvShows() {
  const [channel, setChannel] = useState([]);

  const fetchChannel = async () => {
    try {
      const { data, status } = await axios.get(`channel/all`, {});
      if (status === 200) {
        setChannel(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchChannel();
  }, []);
  return (
    <div>
      <div className="cardss" style={{ marginTop: "150px" }}>
        <h3 className="tv text-white text-start">National Tv</h3>

        <Link to="/addChannel" className="btn btn-success w-75 mx-2 mt-4">
          <i className="fa-solid fa-plus">Add</i>
        </Link>

        <div
          id="carouselExampleControls"
          className="carousel slide"
          data-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="cards-wrapper"></div>
            </div>
            <div className="container movies mt-4 text-light">
              <div className="container">
                <div className="row mt-4">
                  {channel.map((channel, index) => (
                    <div key={index} className="col-lg-3">
                      <TvShow
                        key={channel.id}
                        channel={channel}
                        setChannel={setChannel}
                      />
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TvShows;
