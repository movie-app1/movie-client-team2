import React, { useEffect, useState } from "react";
import { instance as axios } from "../utils/Api";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../Firebase";

function AddChanel() {
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [image, setImage] = useState(null);

  const [channel, setChannel] = useState([]);

  const fetchChannel = async () => {
    try {
      const { data, status } = await axios.get(`channel/all`);
      if (status === 200) {
        setChannel(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchChannel();
  }, []);

  const addChannel = async (downloadURL) => {
    try {
      const formData = {
        title: title,
        image: downloadURL,
      };

      await axios.post(`channel/add`, formData);
      Swal.fire({
        icon: "success",
        title: "success add Channel",
        showConfirmButton: false,
        timer: 1500,
      });
      navigate("/tvShow");
    } catch (err) {
      console.log(err);
    }
  };

  const submit = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${image.name}`);
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          addChannel(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const save = (e) => {
    e.preventDefault();
    submit();
  };
  return (
    <div>
      {" "}
      <div className="container" style={{ marginTop: "150px" }}>
        <form className="" onSubmit={save}>
          <div className="row g-3 mb-md-2 mt-md-2 pb-3">
            <div className="col form-outline form-white mb-4">
              <input
                type="text"
                className="form-control form-control-lg"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
            <div className=" form-outline form-white mb-4">
              <input
                type="file"
                className="form-control form-control-lg"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Image"
                accept="image/png, image/jpeg, image/jpg"
                onChange={(e) => setImage(e.target.files[0])}
              />
            </div>

            <div className="col-12">
              <button
                type="submit"
                className="btn btn-transparent text-light my-4"
              >
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AddChanel;
