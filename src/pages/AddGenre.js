import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../utils/Api";

function AddGenre() {
  const [name, setName] = useState("");
  const navigate = useNavigate();

  const addGenre = async () => {
    try {
      const formData = {
        name: name,
      };

      await axios.post(`categorys/add`, formData, {});
      Swal.fire({
        icon: "success",
        title: "success add Genre",
        showConfirmButton: false,
        timer: 1500,
      });
    } catch (err) {
      console.log(err);
    }
    navigate("/movies");
  };

  const submit = (e) => {
    e.preventDefault();
    addGenre();
  };

  return (
    <div>
      <div className="container" style={{ marginTop: "150px" }}>
        <form className="">
          <div className="row g-3 mb-md-2 mt-md-2 pb-3">
            <div className="col form-outline form-white mb-4">
              <input
                type="text"
                className="form-control form-control-lg"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Genre"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div className="col-12">
              <button
                onClick={submit}
                className="btn btn-transparent text-light my-4"
              >
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AddGenre;
