import "./App.css";
import { Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Footer from "./components/Footer";
import DetailMovie from "./pages/DetailMovie";
import Games from "./pages/Games";
import TvShows from "./pages/TvShows";
import Login from "./pages/Login";
import Register from "./pages/Register";
import AddForm from "./pages/AddForm";
import Edit from "./pages/Edit";
import PrivateRoute from "./components/PrivateRoute";
import Movies from "./pages/Movies";
import AddGame from "./pages/AddGame";
import EditGame from "./pages/EditGame";
import AddGenre from "./pages/AddGenre";
import DetailCategory from "./pages/DetailCategory";
import AddChanel from "./pages/AddChanel";
import EditCannel from "./pages/EditCannel";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />}>
          Home
        </Route>
        <Route path="/login" element={<Login />}>
          Login
        </Route>
        <Route path="/addGame" element={<AddGame />}>
          Add Game
        </Route>
        <Route path="/addChannel" element={<AddChanel />}>
          Add Game
        </Route>
        <Route
          path="/movies"
          element={
            <PrivateRoute>
              <Movies />
            </PrivateRoute>
          }
        >
          Movies
        </Route>
        <Route
          path="/addGenre"
          element={
            <PrivateRoute>
              <AddGenre />
            </PrivateRoute>
          }
        >
          Movies
        </Route>
        <Route
          path="/editGame/:id"
          element={
            <PrivateRoute>
              <EditGame />
            </PrivateRoute>
          }
        >
          Movies
        </Route>
        <Route path="/register" element={<Register />}>
          Register
        </Route>
        <Route
          path="/DetailCategory/:id"
          element={
            <PrivateRoute>
              <DetailCategory />
            </PrivateRoute>
          }
        >
          DetailCategory
        </Route>
        <Route
          path="/detail/:id"
          element={
            <PrivateRoute>
              <DetailMovie />
            </PrivateRoute>
          }
        >
          Detail
        </Route>

        <Route
          path="/editChannel/:id"
          element={
            <PrivateRoute>
              <EditCannel />
            </PrivateRoute>
          }
        >
          Edit Cannel
        </Route>

        <Route
          path="/update/:id"
          element={
            <PrivateRoute>
              <Edit />
            </PrivateRoute>
          }
        >
          Update
        </Route>

        <Route
          path="/games"
          element={
            <PrivateRoute>
              <Games />
            </PrivateRoute>
          }
        >
          Games
        </Route>

        <Route
          path="/tvShow"
          element={
            <PrivateRoute>
              <TvShows />
            </PrivateRoute>
          }
        >
          TvShow
        </Route>

        <Route
          path="/add"
          element={
            <PrivateRoute>
              <AddForm />
            </PrivateRoute>
          }
        >
          Add
        </Route>
        <Route
          path="*"
          element={<h1 className="mt-5 text-white">HALAMAN TIDAK ADA</h1>}
        >
          Add
        </Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
