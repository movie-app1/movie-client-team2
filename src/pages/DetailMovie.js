import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";

import { instance as axios } from "../utils/Api";

import "../Styles/detail.css";
import Swal from "sweetalert2";
import ReactPlayer from "react-player";

function DetailMovie() {
  const { id } = useParams();
  const [movies, setMovies] = useState([]);

  const fetchMovies = async () => {
    try {
      const { data, status } = await axios.get(`menus/id/${id}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      if (status === 200) {
        setMovies(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchMovies();
  }, []);

  return (
    <div id="card-container " data-offset="2" style={{ marginTop: "150px" }}>
      <div id="card">
        <div class="shine">
          <img className="pot" src={movies.image} alt="" />
        </div>
        <div className="laik">
          <b>{movies.rating}% Menyukai ini</b>
        </div>
        <div class="text-light">
          <h1 className="jud">
            {movies.title} <small>({movies.release_year})</small>
          </h1>

          <p className="pon">{movies.description}</p>
          <div
            className="play_btn"
            data-toggle="modal"
            // data-target="#exampleModalCenter"
            data-target=".bd-example-modal-lg"
          >
            <i className="fas  fa-play-circle text-light fa-2xl"></i>
          </div>
          <div
            class="modal fade bd-example-modal-lg"
            id="exampleModalCenter"
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div
              class="modal-dialog modal-dialog-centered modal-lg "
              role="document"
            >
              <div class="modal-content bg-dark ">
                <iframe
                  className="img-fluid  mx-auto d-block"
                  width="100%"
                  style={{ height: "30vw" }}
                  src={movies.trailler}
                  title="YouTube video player"
                  frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowfullscreen
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DetailMovie;
