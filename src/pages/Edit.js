import Swal from "sweetalert2";
import React, { useState, useEffect } from "react";
import { instance as axios } from "../utils/Api.js";
import { useNavigate, useParams } from "react-router-dom";
import { storage } from "../Firebase";
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytes,
} from "firebase/storage";
// import axios from "axios";

function Edit() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [description, setDescription] = useState("");
  const [trailler, setTrailler] = useState("");
  const [release_year, setRelease_year] = useState("");
  const [image, setImage] = useState(null);
  const [editcover, setEditCover] = useState([]);
  const [rating, setRating] = useState("");

  const updateMovie = async (downloadURL) => {
    try {
      const data = {
        title: title,
        image: downloadURL,
        author: author,
        description: description,
        rating: rating,
        release_year: release_year,
        trailler: trailler,
      };
      console.log(data);
      axios.put(`menus/update/${id}`, data);

      const storageRef = ref(storage, `images/${editcover.image}`);

      deleteObject(storageRef);
      Swal.fire("Yes, You are Successful in editing the Book");

      navigate("/movies");
    } catch (err) {
      console.log(err);
    }
  };

  const save = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${image.name}`);
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          updateMovie(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const getById = async () => {
    const { data } = await axios.get(`menus/id/${id}`);

    setTitle(data.title);
    setDescription(data.description);
    setAuthor(data.author);
    setImage(data.image);
    setEditCover(data);
    setRating(data.rating);
    setRelease_year(data.release_year);
    setTrailler(data.trailler);
  };

  useEffect(() => {
    getById();
  }, [id]);

  return (
    <div className="container" style={{ marginTop: "150px" }}>
      <form className="row g-3">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Deskripsi"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </div>
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Author"
            value={author}
            onChange={(e) => setAuthor(e.target.value)}
          />
        </div>
        <div className="input-group mb-3">
          <input
            type="file"
            className="form-control"
            placeholder="Image"
            accept="image/png, image/jpeg, image/jpg"
            onChange={(e) => setImage(e.target.files[0])}
          />
        </div>
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Trailer"
            value={trailler}
            onChange={(e) => setTrailler(e.target.value)}
          />
        </div>
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Release Year"
            value={release_year}
            onChange={(e) => setRelease_year(e.target.value)}
          />
        </div>
        <div className="col form-outline form-white mb-4">
          <input
            type="text"
            className="form-control form-control-lg"
            placeholder="Rating"
            value={rating}
            onChange={(e) => setRating(e.target.value)}
          />
        </div>
      </form>
      <div className="col-12">
        <button onClick={save} className="btn btn-success text-light my-4">
          Update
        </button>
      </div>
    </div>
  );
}

export default Edit;
