import React, { useEffect, useState } from "react";
import { instance as axios } from "../utils/Api";
// import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import "../Styles/navbar.css";

function Navbar() {
  const [genre, setBookGenre] = useState([]);
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  const fetchGenre = async () => {
    try {
      const { data, status } = await axios.get("categorys/all");
      if (status === 200) {
        setBookGenre(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGenre();
  }, []);

  const fetchMovies = async () => {
    try {
      const { data, status } = await axios.get(`menus/id`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      if (status === 200) {
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchMovies();
  }, []);

  return (
    <div className="al">
      <nav className="navbar navbar-expand-lg fixed-top bg-dark p-3">
        <div className="container ">
          <div></div>
          <Link className="navbar-brand" to="/">
            <img
              src="https://www.visionplus.id/statics/app_logo.png"
              alt="Bootstrap"
              width="150"
            />
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link
                  className="nav-link text-light active "
                  aria-current="page"
                  to="/tvShow"
                >
                  <b>Tv Shows</b>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="nav-link text-light active "
                  aria-current="page"
                  to="/movies"
                >
                  <b>Movies</b>
                </Link>
              </li>
              <li className="nav-item dropdown">
                <Link
                  className="nav-link dropdown-toggle text-light"
                  to=""
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <b>Categories</b>
                </Link>

                <ul
                  className="dropdown-content dropdown-menu dropdown-menu-dark "
                  aria-labelledby="navbarDropdown"
                >
                  {genre.map((cat, index) => (
                    <li key={index}>
                      <Link
                        className="dropdown-item"
                        to={`/DetailCategory/${cat.id}`}
                      >
                        <span> {cat.name}</span>
                      </Link>
                    </li>
                  ))}
                  {localStorage.getItem("role") === "admin" ? (
                    <Link
                      className="dropdown-item bg-danger p-2 "
                      to="/addGenre"
                    >
                      <b>
                        <i className="bi bi-plus-circle mx-1"> </i>ADD GENRE
                      </b>
                    </Link>
                  ) : (
                    <></>
                  )}
                </ul>
              </li>

              <li className="nav-item">
                <Link className="nav-link text-light" to="/games">
                  <b>
                    Games<sup>+</sup>
                  </b>
                </Link>
              </li>
            </ul>
            <div>
              {localStorage.getItem("role") === "admin" ? (
                <Link to="/add" className="btn btn-dark w-75 mx-2">
                  <i className="fa-solid fa-plus">Add</i>
                </Link>
              ) : (
                <></>
              )}
            </div>
            <form className="d-flex mx-3" role="search">
              <div className="box">
                <div name="search">
                  <input
                    type="text"
                    className="input"
                    name="txt"
                    autoComplete="off"
                  />
                </div>
                <div className="search mr-0" style={{ marginRight: "0px" }}>
                  <i className="fa-solid fa-magnifying-glass"></i>
                </div>
              </div>
            </form>

            <button
              type="button"
              className="btn rounded-pill fw-bold text-white mx-3 premiumPackage mt-0"
            >
              Premium Package
            </button>
            <Link
              to="/login"
              type="button"
              className="btn rounded-pill fw-bold text-white premiumPackage mt-0"
            >
              Sign In
            </Link>
            <button
              className="btn rounded-pill fw-bold text-white out mt-0"
              onClick={logout}
            >
              <i className="fa-solid fa-right-from-bracket"></i>
            </button>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
