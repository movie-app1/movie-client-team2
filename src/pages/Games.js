import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Game from "../components/Game";
import "../Styles/carouselgames.css";
import { instance as axios } from "../utils/Api";

function Games() {
  const [games, setGames] = useState([]);

  const fetchGames = async () => {
    try {
      const { data, status } = await axios.get(`game/all`, {});
      if (status === 200) {
        setGames(data);
        console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGames();
  }, []);

  return (
    <div>
      <div className="pic-ctn">
        <img
          src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2023-01-17%2012:06:13-banner_(15)1.png"
          alt=""
          className="pic"
        />
        <img
          src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2022-11-04%2014:43:37-banner_(7).png"
          alt=""
          className="pic"
        />
        <img
          src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2023-01-17%2011:01:32-BANNER_(14).png"
          alt=""
          className="pic"
        />
        <img
          src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2022-09-09%2014:51:51-new_banner.png"
          alt=""
          className="pic"
        />
        <img
          src="https://cms-games.mncplus.com/storage/uploaded/img/banner//2022-11-02%2010:54:43-BANNER_(11).png"
          alt=""
          className="pic"
        />
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <div>
        {localStorage.getItem("role") === "admin" ? (
          <Link to="/addGame" className="btn btn-success w-75 mx-2">
            <i className="fa-solid fa-plus">Add</i>
          </Link>
        ) : (
          <></>
        )}
      </div>

      <div>
        {" "}
        <div className="container">
          {games.map((games, index) => (
            <div key={index}>
              <Game key={games.id} games={games} setGames={setGames} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Games;
