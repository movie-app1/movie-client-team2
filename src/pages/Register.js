import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import "../Styles/register.css";
function Register() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userRegister, setUserRegister] = useState({
    email: "",
    password: "",
    role: "",
  });

  const handleOnChange = (e) => {
    setUserRegister((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const register = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`http://localhost:8080/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userRegister),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem("token", data.token);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/home");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <section className="vh-100 gradient-custom">
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-12 col-md-8 col-lg-6 col-xl-5">
              <div
                className="card bg-dark text-white"
                style={{ borderRadius: "1rem" }}
              >
                <div className="card-body p-5 text-center">
                  <div className="mb-md-2 mt-md-1 pb-2">
                    <h2 className="fw-bold mb-2 text-uppercase">Register</h2>
                    <p className="text-white-50 mb-5">
                      Please enter your login and password!
                    </p>
                    <div className="form-outline form-white mb-4">
                      <input
                        type="text"
                        id="username"
                        onChange={handleOnChange}
                        defaultValue={userRegister.username}
                        className="form-control form-control-lg"
                      />
                      <label className="form-label" htmlFor="typeEmailX">
                        username
                      </label>
                    </div>
                    <div className="form-outline form-white mb-4">
                      <input
                        type="password"
                        id="password"
                        onChange={handleOnChange}
                        defaultValue={userRegister.password}
                        className="form-control form-control-lg"
                      />
                      <label className="form-label" htmlFor="typePasswordX">
                        Password
                      </label>
                    </div>
                    <div className="form-outline form-white mb-4">
                      <input
                        type="text"
                        id="role"
                        onChange={handleOnChange}
                        defaultValue={userRegister.role}
                        className="form-control form-control-lg"
                      />
                      <label className="form-label" htmlFor="typeEmailX">
                        Role
                      </label>
                    </div>
                    <button
                      className="btn btn-outline-light btn-lg px-5"
                      onClick={register}
                      type="submit"
                    >
                      Register
                    </button>
                  </div>
                  <div>
                    <p className="mb-0">
                      have an account?
                      <Link to="/login" className="text-white-50 fw-bold">
                        Sign In
                      </Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div>
        <div className="wave"></div>
        <div className="wave"></div>
        <div className="wave"></div>
      </div>
    </div>
  );
}

export default Register;
