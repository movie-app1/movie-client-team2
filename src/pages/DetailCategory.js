import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { instance as axios } from "../utils/Api";

function DetailCategory() {
  const [genreDetail, setGenreDetail] = useState([]);
  const { id } = useParams();

  const fetchGenre = async () => {
    try {
      const { data, status } = await axios.get(`categorys/menu/${id}`);
      if (status === 200) {
        setGenreDetail(data.menu);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGenre();
  }, []);

  return (
    <div className="text-light">
      {genreDetail.map((detCat, index) => (
        <div key={index}>
          <div className="detail " style={{ marginTop: "150px" }}>
            <div className="container mt-5 mb-5 games">
              <div
                className="d-flex justify-content-center row"
                style={{ marginTop: "100px" }}
              >
                <div className="row p-2 bg-white border rounded">
                  <div className="col-md-3 mt-1">
                    <img
                      className="img-fluid img-responsive rounded product-image"
                      src={detCat.image}
                    />
                  </div>
                  <div className="col-md-6 mt-1 align-self-start">
                    <div className="text-start mx-auto text-dark">
                      <p className="fs-5 ">{detCat.title}</p>
                      <p> {detCat.description}</p>
                    </div>
                    <br />
                    <br />
                  </div>
                  <div className="col-md-3 mt-1 align-self-center d-flex justify-content-center">
                    <button className="justify-content-center mx-auto btn btn-warning ">
                      <i className="fa-solid fa-play"> play</i>
                    </button>
                    <Link to={`/detail/${detCat.id}`}>
                      <button className="justify-content-center mx-auto btn btn-success ">
                        <i className="fa-solid fa-circle-info"> detail</i>
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}

export default DetailCategory;
