import React from "react";
import "../Styles/footer.css";
import "../Styles/footer2.css";

function Footer() {
  return (
    <div>
      <div className="poo text-light mt-5">
        <h1>Watch Everywhere</h1>
        <p className="lh-1">
          Stream more than 16.000 hours of Vision+ Originals, various
        </p>
        <p>movies, series, and live national TV channels on your devices.</p>
        <div className="button">
          <button
            type="button"
            className="btn rounded-pill  text-white mx-3 downloadd"
            data-bs-toggle="tooltip"
            data-bs-placement="bottom"
            title=""
          >
            Download APP
          </button>
          <button
            type="button"
            className="btn rounded-pill text-white mx-3 visionn"
          >
            Go to Vision<sup>+</sup> TV
          </button>
        </div>
        <img
          className="mt-5"
          src="https://www.visionplus.id/statics/placement/devices-webd.png"
          alt=""
          width={"75%"}
        />
        <div className="container text-light mt-5">
          <div className="row">
            <div className="col-3">
              <div className="table text-light">
                <img
                  src="https://www.visionplus.id/statics/app_logo.png"
                  alt=""
                  width={"100px"}
                  className="row"
                />
                <div className="row">
                  <div className="col-6">
                    <b className="row mt-4">About Us</b>
                    <b className="row">Media Center</b>
                    <b className="row">Contact Us</b>
                  </div>
                  <div className="col-6">
                    <b className="row mt-4">FAQ</b>
                    <b className="row">Terms & Condition</b>
                    <b className="row">Privacy Policy</b>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-3">
              <p className="text-secondary">Find us on:</p>
              <div className="d-flex justify-content-start mb-4">
                <i className="fa-brands fa-twitter fa-xl mx-2"></i>{" "}
                <i className="fa-brands fa-instagram fa-xl mx-2"></i>{" "}
                <i className="fa-brands fa-facebook fa-xl mx-2"></i>
              </div>
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/2560px-Google_Play_Store_badge_EN.svg.png"
                className="w-50 px-1"
                alt=""
              />
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/2560px-Download_on_the_App_Store_Badge.svg.png"
                className="w-50 px-1"
                alt=""
              />
              <img
                src="https://hellopaisa.co.za/hellopaisa-2021/wp-content/uploads/2021/06/huawei-Badge-Black.png"
                className="w-50 px-1 mt-2"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
