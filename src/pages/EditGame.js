import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytes,
} from "firebase/storage";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { storage } from "../Firebase";
import { instance as axios } from "../utils/Api";

function EditGame() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState(null);
  const [editcover, setEditCover] = useState([]);

  const updateMovie = async (downloadURL) => {
    try {
      const data = {
        title: title,
        image: downloadURL,
        description: description,
      };
      console.log(data);
      axios.put(`game/update/${id}`, data);

      const storageRef = ref(storage, `images/${editcover.image}`);

      //   deleteObject(storageRef);
      Swal.fire("Yes, You are Successful in editing the Book");

      navigate("/games");
    } catch (err) {
      console.log(err);
    }
  };

  const save = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${image.name}`);
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          updateMovie(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const getById = async () => {
    const { data } = await axios.get(`game/id/${id}`);

    setTitle(data.title);
    setDescription(data.description);
    setImage(data.image);
    setEditCover(data);
  };

  useEffect(() => {
    getById();
  }, [id]);
  return (
    <div>
      {" "}
      <div className="container" style={{ marginTop: "150px" }}>
        <form className="">
          <div className="row g-3 mb-md-2 mt-md-2 pb-3">
            <div className="col form-outline form-white mb-4">
              <input
                type="text"
                className="form-control form-control-lg"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
            <div className=" form-outline form-white mb-4">
              <textarea
                type="text"
                className="form-control form-control-lg"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Deskripsi"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>
            <div className=" form-outline form-white mb-4">
              <input
                type="file"
                className="form-control form-control-lg"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-default"
                placeholder="Image"
                accept="image/png, image/jpeg, image/jpg"
                onChange={(e) => setImage(e.target.files[0])}
              />
            </div>
          </div>
        </form>
        <div className="col-12">
          <button
            onClick={save}
            className="btn btn-transparent text-light my-4"
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
}

export default EditGame;
