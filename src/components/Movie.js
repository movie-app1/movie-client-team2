import React from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../utils/Api";

function Movie({ movie }) {
  const removePost = async () => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Yes, delete it!",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`menus/delete/${movie.id}`, {});
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };
  return (
    <div>
      <div class="card text-light bg-dark">
        <img src={movie.image} class="card-img-top" alt="..." />
        <div class="card-body">
          <h5 class="card-title">{movie.title}</h5>
          <p class="card-text">{movie.author}</p>
          <Link to="#" className="btn btn-primary mt-2 mb-4">
            <i class="fa-solid fa-play"></i>
          </Link>

          <Link
            to={`/detail/${movie.id}`}
            className="btn btn-warning mx-2 mt-2 mb-4"
          >
            <i class="fa-solid fa-circle-info"></i>
          </Link>
          <Link
            to={`/update/${movie.id}`}
            className="btn btn-success mt-2 mb-4"
          >
            <i class="fa-solid fa-pen-to-square"></i>
          </Link>
          <div
            onClick={removePost}
            className="btn btn-danger mx-2 mt-2 mb-4 col-lg-3"
          >
            <i className="fa-solid fa-trash"></i>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Movie;
