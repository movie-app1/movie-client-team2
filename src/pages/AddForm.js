import Swal from "sweetalert2";
import React, { useState, useEffect } from "react";
import { instance as axios } from "../utils/Api";
import { useNavigate, Link, navigate } from "react-router-dom";
import { storage } from "../Firebase";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
// import axios from "axios";

const AddForm = () => {
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [image, setImage] = useState(null);
  const [description, setDescription] = useState("");
  const [trailler, setTrailler] = useState("");
  const [release_year, setRelease_year] = useState("");
  const [rating, setRating] = useState("");
  const [selectCategory, setSelectCategory] = useState("");

  const [genre, setBookGenre] = useState([]);

  const fetchGenre = async () => {
    try {
      const { data, status } = await axios.get(`categorys/all`);
      if (status === 200) {
        setBookGenre(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchGenre();
  }, []);

  const addMovies = async (downloadURL) => {
    try {
      const formData = {
        title: title,
        author: author,
        description: description,
        image: downloadURL,
        rating: rating,
        release_year: release_year,
        trailler: trailler,
        category: { id: selectCategory },
      };

      await axios.post(`menus/add`, formData);
      Swal.fire({
        icon: "success",
        title: "success add book",
        showConfirmButton: false,
        timer: 1500,
      });
      navigate("/movies");
    } catch (err) {
      console.log(err);
    }
  };

  const submit = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${image.name}`);
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadURL) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          addMovies(downloadURL);
          console.log(downloadURL);
        });
      });
  };

  const save = (e) => {
    e.preventDefault();
    submit();
  };

  return (
    <div className="container" style={{ marginTop: "150px" }}>
      <form className="" onSubmit={save}>
        <div className="row g-3 mb-md-2 mt-md-2 pb-3">
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Sutrada"
              value={author}
              onChange={(e) => setAuthor(e.target.value)}
            />
          </div>
          <div className=" form-outline form-white mb-4">
            <textarea
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Deskripsi"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
          <div className=" form-outline form-white mb-4">
            <input
              type="file"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Image"
              accept="image/png, image/jpeg, image/jpg"
              onChange={(e) => setImage(e.target.files[0])}
            />
          </div>
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Trailer"
              value={trailler}
              onChange={(e) => setTrailler(e.target.value)}
            />
          </div>
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Release Year"
              value={release_year}
              onChange={(e) => setRelease_year(e.target.value)}
            />
          </div>
          <div className="col form-outline form-white mb-4">
            <select
              className="form-select  form-control-lg "
              aria-label="Default select example"
              name="Genre"
              value={selectCategory}
              onChange={(e) => {
                setSelectCategory(e.target.value.toString());
              }}
              required
            >
              <option selected disabled value={""}>
                Select Genre
              </option>
              {genre.map((cat, index) => (
                <option value={cat.id} key={index}>
                  {cat.name}
                </option>
              ))}
            </select>

            {/* <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Genres"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            /> */}
          </div>
          <div className="col form-outline form-white mb-4">
            <input
              type="text"
              className="form-control form-control-lg"
              aria-label="Sizing example input"
              aria-describedby="inputGroup-sizing-default"
              placeholder="Rating"
              value={rating}
              onChange={(e) => setRating(e.target.value)}
            />
          </div>

          <div className="col-12">
            <button
              onClick={save}
              type="submit"
              className="btn btn-transparent text-light my-4"
            >
              Save
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddForm;
