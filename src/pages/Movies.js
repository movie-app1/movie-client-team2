import React, { useState, useEffect } from "react";
import Movie from "../components/Movie";
import { instance as axios } from "../utils/Api";

function Movies() {
  const [movies, setMovies] = useState([]);

  const fetchMovies = async () => {
    try {
      const { data, status } = await axios.get(`menus/all`, {});
      if (status === 200) {
        setMovies(data);
        // console.log(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchMovies();
  }, []);

  return (
    <div style={{ marginTop: "150px" }}>
      <div className="container movies mt-4 text-light">
        <div className="row">
          {movies.map((movie, index) => (
            <div key={index} className="col-lg-3 item mb-4">
              <Movie key={movie.id} movie={movie} setMovies={setMovies} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Movies;
